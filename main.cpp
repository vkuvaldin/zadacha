#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	ifstream in("214.txt");
	if (in.is_open())
	{
		int Str = 0;
		string str;
		while (getline(in, str))
		{
			int A = 0;
			int E = 0;
			int i = 0;
			while (str[i])
			{
				if (str[i] == 'A')
					A++;
				if (str[i] == 'E')
					E++;
				i++;
			}
			if (E > A)
				Str++;
		}
		cout << "Result: " << Str << endl;
		in.close();
	}
	else
		cout << "No file";
	system("pause");
	return 0;
}